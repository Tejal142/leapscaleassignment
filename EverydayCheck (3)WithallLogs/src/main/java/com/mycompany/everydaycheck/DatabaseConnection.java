/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.everydaycheck;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.FileAppender;

import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

/**
 *
 * @author swapnil
 */
public class DatabaseConnection {

	/**
	 * @param args
	 *            the command line arguments
	 */

	final static Logger log = Logger.getLogger(DatabaseConnection.class);

	public static void main(String[] args) throws Exception {

		Properties logp = new Properties();
		InputStream in = null;
		in = new FileInputStream("/home/rkhandve/Downloads/EverydayCheck (3)/src/main/java/log4j.properties");
		logp.load(in);
		String lo = logp.getProperty("log4j.appender.FILE.layout.conversionPattern");

		log.addAppender(new FileAppender(new PatternLayout(lo), "output.log"));
		/*
		 * log.addAppender( new FileAppender(new
		 * PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n"),
		 * "output.log"));
		 */

		log.info("Welcome in DataBase Connection class");

		log.trace("Tracing message .....................");

		Properties prop = new Properties();
		InputStream input = null;
		input = new FileInputStream("/home/rkhandve/Downloads/EverydayCheck (3)/src/FileProperty/configFile");
		prop.load(input);
		final int time_period = Integer.parseInt(prop.getProperty("time"));
		final String filepath = prop.getProperty("path");
		;
		final String classname = prop.getProperty("class");
		final String username = prop.getProperty("username");
		final String pass = prop.getProperty("pasword");
		final String URL = prop.getProperty("url");

		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				BufferedReader reader;
				// TODO code application logic here
				try {

					reader = new BufferedReader(new FileReader(filepath));

					List<QueryAndResult> queryAndResultList = new ArrayList<>();

					QueryAndResult queryAndResult = new QueryAndResult();
					Class.forName(classname);
					String userName = username;
					String password = pass;
					String url = URL;
					Connection con = DriverManager.getConnection(url, userName, password);
					log.info("Database connection established");
					System.out.println("Database connection established");
					Statement s1;
					ResultSet rs;
					String query;

					String line = reader.readLine();
					while (line != null) {
						System.out.println("Query: " + line);

						queryAndResult = new QueryAndResult();
						s1 = con.createStatement();
						query = line;
						rs = s1.executeQuery(query);
						queryAndResult.setQuery("<br><hr>mysql> " + query);
						String table = "<br><table border=1>";

						ResultSetMetaData rsmd = rs.getMetaData();
						int columnsNumber = rsmd.getColumnCount();
						table += "<tr>";
						System.out.println("Number of columns: " + columnsNumber);
						for (int i = 1; i <= columnsNumber; i++) {
							String name = rsmd.getColumnName(i);
							table += "<th>" + name + "</th>";
						}
						table += "</tr>";

						while (rs.next()) {

							String row = "<tr>";
							for (int i = 1; i <= columnsNumber; i++) {
								row += "<td>" + rs.getString(i) + "</td>";
							}
							row += "</tr>";
							table += row;
						}
						table += "</table>";
						queryAndResult.setResult(table);
						queryAndResultList.add(queryAndResult);
						line = reader.readLine();
					}

					SendEmail.sendMailToAllUsers(queryAndResultList);

				} catch (Exception e) {
					log.error("Exception Occured", e);

					// log.trace("Trace Message ");

					try {
						throw e;
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						log.error("Exception Occured", e);
					}
				}

			}
		};
		ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
		service.scheduleAtFixedRate(runnable, 0, time_period, TimeUnit.HOURS);

	}

}
