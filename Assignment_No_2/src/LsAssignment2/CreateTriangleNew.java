package LsAssignment2;
import java.awt.*;
import javax.swing.*;
import java.awt.geom.Line2D;

@SuppressWarnings("serial")
public class CreateTriangleNew extends JApplet {

	public void init() {
		setBackground(Color.pink.darker());																						// set the background colour
	}

	public void paint(Graphics g) {
		int sa;
		int sb;
		int sc;

		CreateTriangleNew LegenthA = new CreateTriangleNew();																	//Creating the object of CreateTriangle class
		sa = LegenthA.inputA();

		CreateTriangleNew LegenthB = new CreateTriangleNew();
		sb = LegenthB.inputB();

		CreateTriangleNew LegenthC = new CreateTriangleNew();
		sc = LegenthC.inputC();
																																//Creating object of graphics2d
		Graphics2D gra = (Graphics2D) g;
		gra.setFont(new Font("TimesRoman", Font.PLAIN, 20));																	//Set The Font To the message
		 gra.setStroke(new BasicStroke(5));																						//Thickness of Line
		gra.setColor(Color.blue);		
		gra.drawString("Side A:" + sa, 10, 40);
		gra.drawLine(190, (sa + 100), (190 + (sc / 2)), 100); 																	// side a
		
		
		gra.setColor(Color.red);
		gra.drawString("side B:"+sb, 10, 60);
		gra.drawLine((190 + sc), (sb + 100), (190 + (sc / 2)), 100); 															// side b

		gra.setColor(Color.green);		
		gra.drawString("Side C" +sc, 10, 80);
		gra.drawLine(190, (sa + 100), sc + 190, sb + 100);																		// side c
		
		if (sa == sb && sb == sc) {
			gra.drawString("This Triangle is a Equilateral Triangle ", 400, 600);
			
		} else if ((sa == sc) || (sb == sc) || (sa == sc)) {
			gra.drawString("This Triangle is a Isocele Triangle ", 400, 600); 
			} else {
				gra.drawString("This Triangle is a Scalen Triangle ", 400, 600);		}
				repaint();
		}

		public int inputA() {
			int sa;
			String SA;
			SA = JOptionPane.showInputDialog("Enther the legenth of side A :");													
			sa = Integer.parseInt(SA);
			return sa;
		}

		public int inputB() {
			int sb;
			String SB;
			SB = JOptionPane.showInputDialog("Enter the legenth of side B: ");
			sb = Integer.parseInt(SB);
			return sb;
		}

		public int inputC() {
			int sc;
			String SC;
			SC = JOptionPane.showInputDialog("Enter the legenth of side C: ");
			sc = Integer.parseInt(SC);
			return sc;
		}

}