/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.everydaycheck;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author swapnil
 */
public class DatabaseConnection {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        BufferedReader reader;
        // TODO code application logic here
        try {

            reader = new BufferedReader(new FileReader("/home/adashwant/deleteThis/EverydayScriptInput/necessaryTest.sql"));

            List<QueryAndResult> queryAndResultList = new ArrayList<>();

            QueryAndResult queryAndResult = new QueryAndResult();
            Class.forName("com.mysql.jdbc.Driver");
            String userName = "rm_read";
            String password = "Bhy8WmqB";
            String url = "jdbc:mysql://driverconnecttest.cucxrj3bkwnv.us-east-1.rds.amazonaws.com:3306/rm_integrated";
            Connection con = DriverManager.getConnection(url, userName, password);
            System.out.println("Database connection established");
            Statement s1;
            ResultSet rs;
            String query;

            String line = reader.readLine();
            while (line != null) {
                System.out.println("Query: " + line);

                queryAndResult = new QueryAndResult();
                s1 = con.createStatement();
                query = line;
                rs = s1.executeQuery(query);
                queryAndResult.setQuery("<br><hr>mysql> " + query);
                String table = "<br><table border=1>";

                ResultSetMetaData rsmd = rs.getMetaData();
                int columnsNumber = rsmd.getColumnCount();
                table += "<tr>";
                System.out.println("Number of columns: " + columnsNumber);
                for (int i = 1; i <= columnsNumber; i++) {
                    String name = rsmd.getColumnName(i);
                    table += "<th>" + name + "</th>";
                }
                table += "</tr>";

                while (rs.next()) {

                    String row = "<tr>";
                    for (int i = 1; i <= columnsNumber; i++) {
                        row += "<td>" + rs.getString(i) + "</td>";
                    }
                    row += "</tr>";
                    table += row;
                }
                table += "</table>";
                queryAndResult.setResult(table);
                queryAndResultList.add(queryAndResult);
                line = reader.readLine();
            }

            SendEmail.sendMailToAllUsers(queryAndResultList);

        } catch (Exception e) {
            System.out.println("Exception occured: " + e);
            throw e;
        }
    }

}
