package InputController.Com;

import java.util.Arrays;
import java.util.Scanner;

import ModelClass.Com.MinMax;
import ModelClass.Com.Sorting;


public class Myscanner {

	public Myscanner()
	{
	int n;
	Scanner s = new Scanner(System.in);										// create object of scanner.
	System.out.println("Enter number of elements you wants to enter :");	// you have to enter number here.
	n = s.nextInt();														// read entered number and store it in "n".
	int arr[] = new int[n];
	int size = arr.length;													// find out the length of array.

	for (int i = 0; i < arr.length; i++) 
	{
		System.out.print("Enter [" + (i + 1) + "] element :");
		arr[i] = s.nextInt();
	}
	int low = 0;
	int high = arr.length - 1;
	MinMax Q = new MinMax();												// create object of MinMax Class.
	Sorting sr=new Sorting();												// create object of Sorting Class.
	sr.quickSort(arr, low, high);
	System.out.println(Arrays.toString(arr));
	System.out.print("Maximum value is :" + Q.getMax(arr, size));
	System.out.print("Minimum value is :" + Q.getMin(arr, size));
	
	}
}
