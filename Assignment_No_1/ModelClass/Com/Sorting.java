package ModelClass.Com;

public class Sorting 
{
	public void quickSort(int[] arr, int low, int high) 
	{
		if (arr == null || arr.length == 0)
			return;

		if (low >= high)
			return;	
																								
		int middle = low + (high - low) / 2;									// pick the pivot			
		int pivot = arr[middle];
		int i = low, j = high;													// make left < pivot and right > pivot
		while (i <= j) 
		{
			while (arr[i] < pivot) 
			{
				i++;
			}
			while (arr[j] > pivot) 
			{
				j--;
			}
			if (i <= j) {
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
				i++;
				j--;
			}
		 }
																				// recursively sort two sub parts
	    if (low < j)
			quickSort(arr, low, j);

		if (high > i)
			quickSort(arr, i, high);
	}
}
