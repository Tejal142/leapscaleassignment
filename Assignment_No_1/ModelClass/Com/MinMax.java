package ModelClass.Com;


public class MinMax 

{																			
	private int minValue;													//Declare minValue Variable
	private int maxValue;													//Declare maxValue Variable

	public int getMinValue() 
	{
		return minValue;
	}

	public void setMinValue(int minValue) 
	{
		this.minValue = minValue;
	}

	public int getMaxValue() 
	{
		return maxValue;
	}

	public void setMaxValue(int maxValue)
	{
		this.maxValue = maxValue;
	}

	public MinMax() 
	{
		
	}

	public MinMax(int minValue, int maxValue)	
	{
		super();
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	public int getMax(int[] arr, int size)										//Create Method for Find maximum value
	{
		{
			maxValue = arr[0];

			for (int i = 1; i < arr.length; i++) 
			{
				if (arr[i] > maxValue) 
				{
					maxValue = arr[i];

				}
			}

			return maxValue;

		}
	}

	public int getMin(int[] arr, int size) 										//Create Method for Find minimum value
	{
		minValue = arr[0];

		for (int i = 1; i < arr.length; i++) 
		{
			if (arr[i] < minValue) {
				minValue = arr[i];
			}

		}
		return minValue;
	}

}
